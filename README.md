# therapi

Thermostat with a Rapberry Pi

# Installation

Follow instructions to make and install BCM 2835 library needed by the https://www.npmjs.com/package/node-dht-sensor module to operate the DHT22 temperature and humidity sensor.
Instructions can be found at http://www.airspayce.com/mikem/bcm2835/

Install mongoDB database to keep information about current system config and heater schedules.

`sudo apt install mongodb`