import gpio from 'onoff'
import sensor from 'node-dht-sensor'

const SENSOR_TYPE = 22
const TEMP_SENSOR_GPIO = 13
const RELAY_GPIO = 26

class HardwareController {
    constructor() {
        this.relay = new gpio.Gpio(RELAY_GPIO, 'out')
        this.temp = TEMP_SENSOR_GPIO
        this.targetTemp = 0
    }

    setHeaterOn(onStatus) {
        let write = onStatus ? 1 : 0
        console.log(`changed heater status to ${write}`)
        this.relay.writeSync(onStatus ? 1 : 0)
    }

    isHeaterOn() {
        return this.relay.readSync() === 1
    }

    readTemp(callback) {
        sensor.read(SENSOR_TYPE, this.temp, callback);    
    }
}

const TEMP_INC = 0.05

class FakeHardwareController {
    constructor() {
        this.temp = 19
        this.onStatus = false
        this.targetTemp = 0
        this.intervalId = setInterval(() => this.updateTemp(), 1000)   
    }

    updateTemp() {
        if (this.onStatus) {
            this.temp += TEMP_INC
        } else if (this.temp > 16) {
            this.temp -= TEMP_INC
        }
    }

    setHeaterOn(onStatus) {
        this.onStatus = onStatus
    }

    isHeaterOn() {
        return this.onStatus
    }

    readTemp(callback) {
        callback(undefined, this.temp, 60)
    }
}

const instance = new HardwareController()

export default instance