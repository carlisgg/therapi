import Vue from 'vue'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import App from './components/App.vue'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css' 

axios.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem('user-token');

    if (token) {
      config.headers['Authorization'] = `Bearer ${ token }`
    }

    return config
  }, 

  (error) => {
    return Promise.reject(error);
  }
)

const opts = {
  isDark: false,
  icons: {
    iconfont: 'md',
  }
}

Vue.use(Vuetify, VueAxios, axios)

new Vue({
  router,
  vuetify: new Vuetify(opts),
  el: 'app',
  created: function () {
    console.log('created')
  },
  components: {App},
  methods: {}
})