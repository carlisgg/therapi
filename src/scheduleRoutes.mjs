import express from 'express'
import Schedules from './persistence/Schedules'
import heater from './heater'

let router = express.Router()
let schedules = new Schedules()

router.get("/", (req, res) => {
    schedules.allSchedules().then(schedules => res.status(200).json(schedules))
});

router.post("/", (req, res) => {
  let jsonSchedule = req.body.data

  schedules.addSchedule(jsonSchedule).then((scheduleId) => {
    heater.loadSchedules()
    res.status(200).send(scheduleId)
  }, (error) => {
    res.sendStatus(400)
  })
})

router.delete("/:id", (req, res) => {
  schedules.deleteSchedule(req.params.id)
  heater.loadSchedules()
  res.status(200).send()
});
  
export default router