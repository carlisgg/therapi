import passport from 'passport'
import passportLocal from 'passport-local'
import passportJWT from 'passport-jwt'
import bcrypt from 'bcrypt'
import Users from './persistence/Users'

const LocalStrategy = passportLocal.Strategy
const JWTStrategy = passportJWT.Strategy
const ExtractJWT = passportJWT.ExtractJwt

let BCRYPT_SALT_ROUNDS = 12
let users = new Users()

passport.use(new LocalStrategy((username, password, done) => {
    users.findUser(username).then((user) => {
      bcrypt.compare(password, user.password).then((valid) => {
        done(null, valid && user)
      }, (error) => {
        done(null, false)  
      })
    }, (error) => {
      done(null, false)
    })
  }
))

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : 'your_jwt_secret'
    }, (jwtPayload, cb) => {
        return cb(null, jwtPayload)
    }
))

export default passport