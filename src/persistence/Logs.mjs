import getDB from './Database'

class Logs {
    constructor() {
        getDB((err, dbConn) => this.db = dbConn)
    }

    addHeaterStatusChanged(newStatus) {
        this.db.collection('logs').insertOne({
            type: 'heater',
            status: newStatus,
            ts: new Date().getTime()
        })
    }

    addTempChanged(newTemp) {
        this.db.collection('logs').insertOne({
            type: 'temp',
            value: newTemp,
            ts: new Date().getTime()
        })
    }

    heaterLogs(from, to) {
        return this.db.collection('logs').find({ type: 'heater', ts: { $gt: from, $lt: to } }).toArray()
    }

    temperatureLogs(from, to) {
        return this.db.collection('logs').find({ type: 'temp', ts: { $gt: from, $lt: to } }).toArray()
    }
}

export default Logs