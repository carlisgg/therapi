import getDB from './Database'
import bcrypt from 'bcrypt'

let BCRYPT_SALT_ROUNDS = 12

class Users {
    constructor() {
        getDB((error, dbConn) => {
            console.log('init db')
            this.db = dbConn
            this.initAdminUser()
        })
    }

    initAdminUser() {
        this.db.collection('users').findOne({username: 'admin'}).then(
            (value) => {
                if (!value) {
                    bcrypt.hash('admin', BCRYPT_SALT_ROUNDS).then((passwordHash) => {
                        this.db.collection('users').insertOne({
                            username: 'admin',
                            password: passwordHash
                        })    
                    })
                }
            }
        )
    }

    updatePassword(username, newPassword) {
        bcrypt.hash(newPassword, BCRYPT_SALT_ROUNDS).then((passwordHash) => {
            this.db.collection('users').updateOne(
                { 'username': username },
                { $set: { 'password': passwordHash }}
            )    
        })
    }

    findUser(username) {
        return this.db.collection('users').findOne({'username': username})
    }

    findAll() {
        return this.db.collection('users').find({}).toArray()
    }
}

export default Users