import getDB from './Database'
import mongo from 'mongodb'

class Schedules {
    constructor() {
        getDB((error, dbConn) => {
            this.db = dbConn
        })
    }

    addSchedule(schedule) {
        return this.db.collection('schedules').insertOne(schedule)
    }

    deleteSchedule(scheduleId) {
        console.log("deleting", scheduleId)
        this.db.collection('schedules').deleteOne(
            { '_id': mongo.ObjectId(scheduleId) }
        )
    }

    allSchedules() {
        return this.db.collection('schedules').find({}).limit(100).toArray()
    }
}

export default Schedules