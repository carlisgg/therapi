import getDB from './Database'

class System {
    constructor() {
        getDB((err, dbConn) => this.db = dbConn)
    }

    powerOff() {
        this.db.collection('system').updateMany({}, {$set: {power: "off"}})
    }

    lastConfig() {
        return this.db.collection('system').find({}).limit(1).toArray()
    }
}

export default System