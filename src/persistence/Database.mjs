import MongoClient from 'mongodb'
import config from '../../config'

let currentDb = undefined

let getDB = (callback) => {
    if (currentDb) {
        callback(null, currentDb)
    } else {
        let connURL = config.database
        MongoClient.connect(connURL, (err,db) => {
            if (err) {
                console.log(`Failed to connect to the database. ${err.stack}`)
            } else {
                currentDb = db
                console.log("created new DB connection")
            }

            callback(err, currentDb)
        })
    }
}

export default getDB