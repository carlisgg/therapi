import config from '../config'
import hardwareController from './hardware'
import Logs from './persistence/Logs'
import Schedules from './persistence/Schedules'
import scheduler from 'node-schedule'

const HEATER_CHECK_INTERVAL_MILLIS = config.heaterCheckIntervalMillis
const TEMP_LOG_INTERVAL_MILLIS = config.temperatureLoggingIntervalMinutes * 60 * 1000

class Heater {
    constructor() {
        this.logger = new Logs()
        this.schedules = new Schedules()
        this.jobs = []

        this.lastTempLogMillis = new Date().getTime()
        this.mode = config.mode
        this.triggerOffset = config.triggerOffset
        this.targetTemp = config.minTemp
        this.offTimer = {
            timerId: undefined,
            timerOffTime: undefined
        }
        this.powerSystem('on')
        this.loadSchedules()
    }

    loop() {
        this.loopId = setInterval(() => {
            let heaterOnStatus = hardwareController.isHeaterOn()
            hardwareController.readTemp((err, temperature, humidity) => {
                if (!err) {
                    let realTemp = Math.round(temperature * 100) / 100
                    this.logTemp(realTemp)
                    console.log(`...real temp is ${realTemp} and target temp is ${this.targetTemp}`)
                    
                    if (realTemp >= this.targetTemp && heaterOnStatus) {
                        console.log('...switching heater off')
                        hardwareController.setHeaterOn(false)
                        this.logger.addHeaterStatusChanged('off')
                        this.triggerOffset = config.triggerOffset
                    }

                    if (realTemp < (this.targetTemp - this.triggerOffset) && !heaterOnStatus) {
                        console.log('...switching heater on')
                        hardwareController.setHeaterOn(true)
                        this.logger.addHeaterStatusChanged('on')
                        this.triggerOffset = 0
                    }
                }
            })
        }, HEATER_CHECK_INTERVAL_MILLIS)
    }

    logTemp(tempValue) {
        let currentTimeMillis = new Date().getTime()
        if (this.lastTempLogMillis + TEMP_LOG_INTERVAL_MILLIS < currentTimeMillis) {
            console.log('logging temp ', tempValue)
            this.logger.addTempChanged(tempValue)
            this.lastTempLogMillis = currentTimeMillis
        }
    }

    powerSystem(powerStatus) {
        if (this.power === powerStatus) {
            return
        }
        
        this.power = powerStatus
        if (powerStatus === 'on') {
            console.log('powering system on...')
            this.loop()
        } else {
            console.log('powering system off...')
            this.unset()

            if (this.loopId) {
                clearInterval(this.loopId)
                this.loopId = undefined
            }
        }
    }

    unset() {
        hardwareController.setHeaterOn(false)
        this.targetTemp = config.minTemp
        
        if (this.offTimer.timerId) {
            clearTimeout(this.offTimer.timerId)
            this.offTimer.timerId = undefined
            this.offTimer.timerOffTime = undefined
        }

        this.jobs.forEach(job => job.cancel())
    }

    manualMode() {
        if (this.mode !== 'manual') {
            this.mode = 'manual'
            this.unset()
        }
    }

    scheduledMode() {
        if (this.mode !== 'schedule') {
            this.mode = 'schedule'
            this.unset()

            this.loadSchedules()
        }
    }

    /*
      {  
        day: 0,
        start: {
          hour: 0,
          minute: 0
        },
        end: {
          hour: 0,
          minute: 0
        },
        targetTemp: 0
      }
    */
    loadSchedules() {
        if (this.mode !== 'schedule') {
            return
        }

        this.schedules.allSchedules().then(schedules => 
            schedules.forEach(entry => {
                let newJob = scheduler.scheduleJob(`${entry.start.minute} ${entry.start.hour} * * ${entry.day}`, () => {
                    let startMinute = entry.start.hour * 60 + entry.start.minute
                    let endMinute = entry.end.hour * 60 + entry.end.minute

                    let duration = endMinute - startMinute
                    this.setTarget(entry.targetTemp, duration)
                })

                this.jobs.push(newJob)
                console.log("created scheduled job", newJob)
            })
        )
    }

    setTarget(temp, offIntervalMins) {
        if (temp && temp > 0) {
            this.targetTemp = temp
            this.triggerOffset = 0
        }
        
        if (this.offTimer.timerId) {
            clearTimeout(this.offTimer.timerId)
            this.offTimer.timerId = undefined
            this.offTimer.timerOffTime = undefined
        }

        if (offIntervalMins && offIntervalMins > 0) {
            this.offTimer.timerId = setTimeout(() => this.unset(), offIntervalMins * 60 * 1000)
            this.offTimer.timerOffTime = new Date().getTime() + offIntervalMins * 60 * 1000
        }
    }

    getTimeToOff() {
        if (this.mode === 'manual' && this.offTimer.timerOffTime && this.offTimer.timerOffTime > 0) {
            return this.offTimer.timerOffTime - new Date().getTime()
        }

        return false
    }

    getStatus() {
        return {
            mode: this.mode,
            targetTemp: this.targetTemp,
            heaterOnStatus: hardwareController.isHeaterOn(),
            timeToOff: this.getTimeToOff(),
            power: this.power
        }
    }
}

const instance = new Heater();

export default instance;