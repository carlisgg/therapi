import express from 'express'
import Logs from './persistence/Logs.mjs';

let router = express.Router()

router.get("/heater", (req, res) => {
    let from = parseInt(req.query.from) || 0
    let to = parseInt(req.query.to) || new Date().getTime()

    let logger = new Logs()
    logger.heaterLogs(from, to).then(logs => {
        console.log(logs.length)
        res.status(200).json(logs)
    })
});

router.get("/temp", (req, res) => {
    let from = parseInt(req.query.from) || 0
    let to = parseInt(req.query.to) || new Date().getTime()

    let logger = new Logs()
    logger.temperatureLogs(from, to).then(logs => {
        console.log(logs.length)
        res.status(200).json(logs)
    })
});

export default router