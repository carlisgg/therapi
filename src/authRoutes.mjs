import express from 'express'
import passport from 'passport'
import jwt from 'jsonwebtoken'
import Users from './persistence/Users';

let router = express.Router()
let users = new Users()

router.post('/login', (req, res, next) => {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: 'Something is not right',
                user   : user
            })
        }

       req.login(user, {session: false}, (err) => {
           if (err) {
               res.send(err)
           }

           const token = jwt.sign(user, 'your_jwt_secret')
           return res.json({user, token})
        })
    })(req, res)
})

router.post('/password', passport.authenticate('jwt', {session: false}), (req, res) => {
    console.log('request', req.user)
    let jsonBody = req.body;
    users.findUser(req.user.username).then((user) => {
        if (user.password === jsonBody.currentPassword) {
            let updated = users.updatePassword(user.username, jsonBody.newPassword)
            res.json(updated)
        } else {
            res.status(401).send('Invalid current password')
        }
    }, (error) => {
        res.status(404).send(error)
    })
})

export default router