import express from 'express'
import heater from "./heater"

let router = express.Router()

router.get("/status", (req, res) => {
    let jsonRes = heater.getStatus()
    res.status(200).json(jsonRes)
})
  
router.post("/status", (req, res) => {
    let jsonBody = req.body
    heater.powerSystem(jsonBody.status)

    res.json("changed heater status to " + jsonBody.status)
})

router.post("/mode", (req, res) => {
  let jsonBody = req.body
  if (jsonBody.mode === 'manual') {
    heater.manualMode()
  }

  if (jsonBody.mode === 'schedule') {
    heater.scheduledMode()
  }

  res.json("changed mode to " + jsonBody.mode)
})

router.post("/params", (req, res) => {
  let jsonBody = req.body
  heater.setTarget(jsonBody.targetTemp, jsonBody.offInterval)

  res.json("changed params to " + jsonBody)
})

export default router