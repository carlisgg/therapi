import express from 'express'
import bodyParser from 'body-parser'
import passport from './passport'

import http from 'http'
import path from 'path'
import hardwareController from './hardware'

import config from '../config'
import heaterRoutes from './heaterRoutes'
import scheduleRoutes from './scheduleRoutes'
import statsRoutes from './statsRoutes'
import authRoutes from './authRoutes'

let app = express()
let __dirname = path.resolve()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(passport.initialize())

// Sends static files from the public path directory
app.use(express.static(__dirname + '/dist/public'))
app.use("/assets", express.static(__dirname + "/public/assets"));

app.use('/auth', authRoutes)
app.use('/heater', passport.authenticate('jwt', {session: false}), heaterRoutes)
app.use('/schedules', passport.authenticate('jwt', {session: false}), scheduleRoutes)
app.use('/stats', passport.authenticate('jwt', {session: false}), statsRoutes)

app.get("/", (req, res) => {
  res.status(200).sendFile('public/index.html',  { root: __dirname })
})

app.get("/sensors/temperature", passport.authenticate('jwt', {session: false}), (req, res) => {
  hardwareController.readTemp((err, temperature, humidity) => {
    if (!err) {
      res.status(200).json({ temp: temperature, hum: humidity })
    } else {
      res.status(503).send("Error")
    }
  })
})

http.createServer(app).listen(8080, (err) => {
  console.log("Server started a 8080")
})