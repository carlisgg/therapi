import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import Profile from './components/Profile.vue'
import Stats from './components/Stats.vue'

Vue.use(VueRouter)

const ifAuthenticated = (to, from, next) => {
    if (localStorage.getItem('user-token')) {
      next()
      return
    }

    next('/login')
  }

const routes = [
    { 
      path: '/', 
      component: Home,
      beforeEnter: ifAuthenticated,
    },
    { 
      path: '/profile', 
      component: Profile,
      beforeEnter: ifAuthenticated,
    },
    { 
      path: '/stats', 
      component: Stats,
      beforeEnter: ifAuthenticated,
    },
    { 
      path: '/login', 
      component: Login 
    }
  ]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router