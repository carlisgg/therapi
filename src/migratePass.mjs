import getDB from './persistence/Database'
import Users from './persistence/Users'

let users = new Users()

getDB((err, conn) => {
    users.findAll().then(userList => {
        userList.forEach(user => {
            console.log('updating', user)
            users.updatePassword(user.username, user.password)      
        })
    })
})